<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SearchTest extends TestCase
{

    public function testExplicabo()
    {
        $response = $this->post('articles/search',['query'=>'Explicabo']);
        $response = json_decode($response->getContent());
        $this->assertEquals(2,count($response));
    }

    public function testDolore()
    {
        $response = $this->post('articles/search',['query'=>'Dolore']);
        $response = json_decode($response->getContent());
        $this->assertEquals(3,count($response));
    }
}
