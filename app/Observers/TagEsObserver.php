<?php

namespace App\Observers;
use App\Models\Tag;

/**
* Listens for eloquent events fired by the model.
* Makes changes to elasticsearch indexes according to changes in the application database.
*
*/
class TagEsObserver
{
    public function updated(Tag $tag)
    {
        foreach ($tag->articles as $article)
        {
            $article->reindex();
        }
    }

    public function deleted(Tag $tag)
    {
        foreach ($tag->articles as $article)
        {
            $article->reindex();
        }
    }
}