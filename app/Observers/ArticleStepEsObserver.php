<?php

namespace App\Observers;
use App\Models\ArticleStep;

/**
* Listens for eloquent events fired by the model.
* Makes changes to elasticsearch indexes according to changes in the application database.
*
*/
class ArticleStepEsObserver
{
    public function created(ArticleStep $articleStep)
    {
        $articleStep->article->reindex();
    }

    public function updated(ArticleStep $articleStep)
    {
        $articleStep->article->reindex();
    }

    public function deleted(ArticleStep $articleStep)
    {
        $articleStep->article->reindex();
    }
}