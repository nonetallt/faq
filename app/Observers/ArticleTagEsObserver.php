<?php

namespace App\Observers;
use App\Models\ArticleTag;

/**
* Listens for eloquent events fired by Tag model.
* Makes changes to elasticsearch indexes according to changes in the application database.
*
*/
class ArticleTagEsObserver
{
    public function created(ArticleTag $tag)
    {
        $tag->article->reindex();
    }

    public function updated(ArticleTag $tag)
    {
        $tag->article->reindex();
    }
}