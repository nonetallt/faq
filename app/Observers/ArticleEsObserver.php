<?php

namespace App\Observers;
use App\Models\Article;

/**
* Listens for eloquent events fired by Article model.
* Makes changes to elasticsearch indexes according to changes in the application database.
*
*/
class ArticleEsObserver
{

    public function created(Article $article)
    {
        $article->addToIndex();
    }

    public function updated(Article $article)
    {
        $article->reindex();
    }

    public function deleted(Article $article)
    {
        $article->deleteIndex();
    }
}