<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tag;

class TagController extends Controller
{

    public function all()
    {
        return Tag::get()->toArray();
    }

    public function create(\App\Http\Requests\CreateTagRequest $request)
    {
        $tag = new Tag;
        $tag->name = $request['name'];
        $tag->description = $request['description'];
        $tag->save();
        return $tag->toArray();
    }

    public function delete(\App\Http\Requests\DeleteTagRequest $request)
    {
        $tag = Tag::find($request['name']);
        $tag->delete();
        return response('Tag deleted successfully.',200);
    }

    public function update(\App\Http\Requests\UpdateTagRequest $request)
    {
        $tag = Tag::find($request['name']);
        $tag->description = $request['description'];
        $tag->save();
        return response('Tag updated successfully.',200);
    }
}