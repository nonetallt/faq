<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ArticleStep;
use Illuminate\Support\Facades\Storage;

class ArticleStepController extends Controller
{
    public function create(\App\Http\Requests\CreateArticleStepRequest $request)
    {
        $step = new ArticleStep;

        if($request->hasFile('picture'))
        {
            $path = Storage::disk('public')->putFile('pictures', $request->file('picture'));
            $step->picture = $path;
        }
        $step->number = $request['number'];
        $step->content = $request['content'];
        $step->article_id = $request['article_id'];
        $step->save();
        return $step->toArray();
    }

    public function update(\App\Http\Requests\UpdateArticleStepRequest $request)
    {
        $article = ArticleStep::find($request['id']);
        $article->update($request->all());
        return response('Article step updated successfully.',200);
    }

    public function delete(\App\Http\Requests\DeleteArticleStepRequest $request)
    {
        $step = ArticleStep::find($request['id']);
        $picture = $step->picture;

        // Update numbers.
        ArticleStep::where('article_id',$step->article_id)
        ->where('number','>',$step->number)
        ->decrement('number');

        $step->delete();

        // Remove image.
        Storage::disk('public')->delete($picture);

        return response('Article step deleted successfully.',200);
    }
}
