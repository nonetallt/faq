<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;

class ArticleController extends Controller
{
    // Define middleware.
    public function __construct()
    {

    }

    public function statistics()
    {
        return response()->json(
        [
            'complete' => Article::where('saved',true)->count(),
            'editing' => Article::where('saved',false)->count()
        ]);
    }

    public function my()
    {
        // Placeholder.
        return Article::get()->toArray();
    }

    public function show($id)
    {
        // Find article and append its steps.
        $article = Article::find($id);

        if(is_null($article))
        {
            return response('Article does not exist.',404);
        }

        $steps = \App\Models\ArticleStep::where('article_id',$article->id)
        ->orderBy('number','asc')
        ->get()
        ->toArray();
        $article = $article->toArray();
        $article['steps'] = $steps;
        return $article;
    }

    public function newest(int $limit = 5)
    {
        return Article::limit($limit)
        ->where('saved',true)
        ->latest()
        ->get()
        ->toArray();
    }

    // Fetch last updated articles.
    public function updated(int $limit = 5)
    {
        return Article::limit($limit)
        ->where('saved',true)
        ->orderBy('updated_at','desc')
        ->get()
        ->toArray();
    }

    /**
    * Get information regarding the article object.
    *
    */
    public function query(\App\Http\Requests\QueryArticleRequest $request)
    {
        $columns = $request['columns'];
        $appendTags = array_search('tags', $columns);

        // Do not try direct selection of tags.
        if($appendTags !== false)
        {
            // Unset found selection key for 'tags'.
            unset($columns[$appendTags]);
        }
        if(empty($columns))
        {
            $columns = ['id'];
        }

        $article = Article::where('id',$request['id'])
        ->select($columns)
        ->first();

        if($appendTags !== false)
        {
            // Append tags for article.
            $article['tags'] = $article->tags->toArray();
        }

        return $article->toArray();
    }


    public function search(\App\Http\Requests\SearchArticlesRequest $request)
    {
        return  Article::search($request['query'])->filter(function($value, $key)
        {
            return $value->saved;
        }
        )->toArray();
    }

    public function tagsFor(\App\Http\Requests\TagsForArticleRequest $request)
    {
        $article = Article::where('id',$request['id'])->select('id')->first();
        return $article->tags->toArray();
    }

    public function attachTag(\App\Http\Requests\AttachTagToArticleRequest $request)
    {
        // Check that this tag is not already attached.
        $attachment = \App\Models\ArticleTag::where('article_id',$request['article'])
        ->where('tag_name',$request['tag'])->first();

        if($attachment !== null)
        {
            return response('Specified tag is already attached to this article.',422);
        }

        $attach = new \App\Models\ArticleTag;
        $attach->article_id = $request['article'];
        $attach->tag_name = $request['tag'];
        $attach->save();
        return response('Successfully attached tag to article.',200);
    }

    public function update(\App\Http\Requests\UpdateArticleRequest $request)
    {
        $article = Article::find($request['id']);
        $article->update($request->all());
        return response('Article updated successfully.',200);
    }

    public function new(\App\Http\Requests\CreateArticleRequest $request)
    {
        $article = new Article;

        // Set placeholder values.
        $article->title = 'The problem in one sentence';
        $article->description = 'Describe the problem this article is ment to solve.';
        $article->platform = 'Windows 8.1';
        $article->save();
        return response()->json(['id' => $article->id]);
    }
}