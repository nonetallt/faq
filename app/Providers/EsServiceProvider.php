<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Article;
use App\Models\Tag;
use App\Models\ArticleStep;
use App\Models\ArticleTag;
use App\Observers\ArticleEsObserver;
use App\Observers\ArticleStepEsObserver;
use App\Observers\ArticleTagEsObserver;
use App\Observers\TagEsObserver;

class EsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Article::observe(ArticleEsObserver::class);
        Tag::observe(TagEsObserver::class);
        ArticleStep::observe(ArticleStepEsObserver::class);
        ArticleTag::observe(ArticleTagEsObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
