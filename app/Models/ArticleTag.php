<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleTag extends Model
{
    public $timestamps = false;

    public function article()
    {
        return $this->belongsTo('App\Models\Article','article_id','id');
    }
}
