<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// Import trait.
use Elasticquent\ElasticquentTrait;

class Tag extends Model
{
    protected $primaryKey = 'name';
    // protected $hidden = ['pivot'];
    public $incrementing = false;

    // Use trait.
    use ElasticquentTrait;

    public function articles()
    {
        return $this->belongsToMany('App\Models\Article', 'article_tags', 'tag_name', 'article_id');
    }
}
