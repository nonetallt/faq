<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Elasticquent\ElasticquentTrait;

class ArticleStep extends Model
{
    use ElasticquentTrait;

    public function article()
    {
        return $this->belongsTo('App\Models\Article','article_id','id');
    }
}