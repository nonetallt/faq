<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// Import trait.
use Elasticquent\ElasticquentTrait;

class Article extends Model
{
    // Use trait.
    use ElasticquentTrait;

    protected $guarded = ['id','created_at','updated_at'];
    protected $casts = ['saved' => 'boolean'];

    protected $mappingProperties =
    [
        'id' =>
        [
            'type' => 'integer'
        ],
        'title' =>
        [
            'type' => 'text'
        ],
        'description' =>
        [
            'type' => 'text'
        ],
        'platform' =>
        [
            'type' => 'keyword'
        ],
        'saved' =>
        [
            'type' => 'boolean'
        ],
        'tags' =>
        [
            'type' => 'nested'
        ],
        'steps' =>
        [
            'type' => 'nested'
        ]
    ];

    public function getIndexDocumentData()
    {
        return
        [
            'id'            => $this->id,
            'title'         => $this->title,
            'description'   => $this->description,
            'platform'      => $this->platform,
            'saved'         => $this->saved ? 'true' : 'false',
            'tags'          => $this->tags,
            'steps'         => $this->steps
        ];
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'article_tags', 'article_id', 'tag_name');
    }

    public function steps()
    {
        return $this->hasMany('App\Models\ArticleStep','article_id','id');
    }
}
