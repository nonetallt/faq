<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use InvalidArgumentException;
use Illuminate\Console\Command;

class ElastiquentAllCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elastiquent:all {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a migration, elastiquent model, resource controller and an observer.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->createMigration();
        $this->createModel();
        $this->createController();
        $this->createObserver();
        $this->line('Remember to add routes and register your observers in a provider class.');
    }

    private function createMigration()
    {
        $table = Str::plural(Str::snake(class_basename($this->argument('name'))));
        $this->call('make:migration', 
        [
            'name' => "create_{$table}_table",
            '--create' => $table,
        ]);
    }

    private function createModel()
    {
        $this->call('elastiquent:model', 
        [
            'name' => $this->argument('name')
        ]);
    }

    private function createController()
    {
        $class = Str::studly(class_basename($this->argument('name')));

        $this->call('make:controller', 
        [
            'name' => "{$class}Controller",
            '--resource' => true,
        ]);
    }

    private function createObserver()
    {
        $class = Str::studly(class_basename($this->argument('name')));

        $this->call('make:observer', 
        [
            'name' => "{$class}EsObserver",
            'model' => $this->argument('name'),
            '--elastiquent' => true
        ]);
    }
}
