<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use InvalidArgumentException;

class ElastiquentModelCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elastiquent:model {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new elastiquent model.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Model';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        if (parent::fire() === false) 
        {
            return;
        }
    }

    /**
     * Get the stub file for the observer.
     *
     * @return string
     */
    protected function getStub()
    {
        // Return the stub with specified model.
        return realpath(__DIR__ . '/..').'/CustomStubs/elastiquent.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Models';
    }

    /**
     * Get the fully-qualified model class name.
     *
     * @param  string  $model
     * @return string
     */
    protected function parseModel($model)
    {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $model)) 
        {
            throw new InvalidArgumentException('Model name contains invalid characters.');
        }

        $model = trim(str_replace('/', '\\', $model), '\\');

        if (! Str::startsWith($model, $rootNamespace = $this->laravel->getNamespace())) 
        {
            $model = $rootNamespace.'Models\\'.$model;
        }

        return $model;
    }
}