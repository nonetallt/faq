<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use InvalidArgumentException;
use Illuminate\Console\GeneratorCommand;

class ModelObserverMakeCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:observer {name} {model} {--elastiquent}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a new observer.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Observer';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        if (parent::fire() === false) 
        {
            return;
        }
    }

    /**
     * Get the stub file for the observer.
     *
     * @return string
     */
    protected function getStub()
    {
        // Use elastiquent update options or plain eloquent models.
        if($this->option('elastiquent'))
        {
            return realpath(__DIR__ . '/..').'/CustomStubs/observer.elastiquent.stub';
        }
        return realpath(__DIR__ . '/..').'/CustomStubs/observer.model.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Observers';
    }

    /**
     * Build the class with the given name.
     *
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        $observerNamespace = $this->getNamespace($name);
        $modelClass = $this->parseModel($this->argument('model'));

        if (! class_exists($modelClass)) 
        {
            if ($this->confirm("A {$modelClass} model does not exist. Do you want to generate it?", true)) 
            {
                $this->call('make:model', ['name' => $modelClass]);
            }
        }

        $replace = 
        [
            'DummyFullModelClass' => $modelClass,
            'DummyModelClass' => class_basename($modelClass),
            'DummyModelVariable' => lcfirst(class_basename($modelClass)),
        ];

        $replace["use {$observerNamespace}\Observer;\n"] = '';

        return str_replace
        (
            array_keys($replace), 
            array_values($replace),
            parent::buildClass($name)
        );
    }

    /**
     * Get the fully-qualified model class name.
     *
     * @param  string  $model
     * @return string
     */
    protected function parseModel($model)
    {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $model)) 
        {
            throw new InvalidArgumentException('Model name contains invalid characters.');
        }

        $model = trim(str_replace('/', '\\', $model), '\\');

        if (! Str::startsWith($model, $rootNamespace = $this->laravel->getNamespace())) 
        {
            $model = $rootNamespace.'Models\\'.$model;
        }

        return $model;
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return 
        [
            ['elastiquent', 'm', InputOption::VALUE_NONE, 'Listen and index model changes in ElasticSearch.']
        ];
    }
}