<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_tags', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->integer('article_id')->unsigned();
            $table->string('tag_name');

            // Add foreign key constraints.
            $table->foreign('article_id')->references('id')->on('articles')
           ->onUpdate('restrict')
           ->onDelete('cascade');

           $table->foreign('tag_name')->references('name')->on('tags')
           ->onUpdate('restrict')
           ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_tags');
    }
}
