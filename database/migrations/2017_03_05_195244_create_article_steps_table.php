<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_steps', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('number')->unsigned();
            $table->string('picture')->nullable();
            $table->string('type')->nullable();
            $table->integer('article_id')->unsigned();
            $table->text('content');
            $table->timestamps();

             // Add foreign key constraints.
             $table->foreign('article_id')->references('id')->on('articles')
            ->onUpdate('restrict')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_steps');
    }
}
