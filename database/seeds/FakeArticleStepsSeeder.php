<?php

use Illuminate\Database\Seeder;

class FakeArticleStepsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $articles = \App\Models\Article::select(['id'])->get();

        foreach ($articles as $article)
        {
            $stepsForArticle = rand(1,3);
            $steps = factory(App\Models\ArticleStep::class, $stepsForArticle)->make();

            foreach ($steps as $index => $step)
            {
                $this->command->info("$article->id : $step->content");

                $step->number = $index;
                $step->article_id = $article->id;
                $step->save();
            }
        }
    }
}
