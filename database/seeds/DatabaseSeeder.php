<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create lorem tags.
        $this->call(FakeTagsSeeder::class);

        // Create lorem articles.
        $this->call(FakeArticlesSeeder::class);

        // Randomly attach tags to articles.
        $this->call(FakeArticleTagsSeeder::class);

        // Create some random steps.
        $this->call(FakeArticleStepsSeeder::class);
    }
}
