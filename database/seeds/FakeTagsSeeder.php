<?php

use Illuminate\Database\Seeder;

class FakeTagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create a collection of fake tags.
        $tags = factory(App\Models\Tag::class, 10)->make();

        // Seed tags to the db.
        foreach ($tags as $tag)
        {
            // Make sure the name is unique.
            $existingTag = \App\Models\Tag::find($tag->name);
            if(is_null($existingTag))
            {
                $tag->save();
            }
        }
    }
}
