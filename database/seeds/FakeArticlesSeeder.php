<?php

use Illuminate\Database\Seeder;

class FakeArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create a collection of fake articles.
        $articles = factory(App\Models\Article::class, 10)->make();

        // Seed articles to the db.
        foreach ($articles as $article)
        {
            $article->save();
        }
    }
}
