<?php

use Illuminate\Database\Seeder;

class FakeArticleTagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $articles = \App\Models\Article::select(['id'])->get();
        $tags = \App\Models\Tag::select(['name'])->get();

        foreach ($articles as $article)
        {
            // Use pool to make sure there are no duplicate attachments for single tag.
            $tagPool = array_flatten($tags->toArray());

            // Attach between 0 to 3 tags.
            $tagsToAttach = rand(0,3);

            for($n = 0; $n < $tagsToAttach; $n++)
            {
                // Select a random tag from the pool.
                $randomId = rand(0,count($tagPool)-1);
                $randomTag = array_splice($tagPool, $randomId, 1)[0];

                $this->command->info("$article->id : $randomTag");

                $articleTag = new \App\Models\ArticleTag;
                $articleTag->tag_name = $randomTag;
                $articleTag->article_id = $article->id;
                $articleTag->save();
            }
        }
    }
}
