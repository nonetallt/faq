<?php

use App\Models\Article;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function()
{
    // dd(Article::rebuildMapping());
    dd(Article::getMapping());
    dd(Article::search('Explicabo'));
    dd(\App\Models\Article::find(1)->toArray());
});

// SPA home.
Route::get('/', function()
{
    return view('layouts.home');
});

// Articles
Route::get('articles/statistics','ArticleController@statistics');
Route::get('articles/article/{id}','ArticleController@show');
Route::get('articles/newest/{limit?}','ArticleController@newest');
Route::get('articles/updated/{limit?}','ArticleController@updated');
Route::get('articles/my','ArticleController@my');
Route::post('articles/tags','ArticleController@tagsFor');
Route::post('articles/query', 'ArticleController@query');
Route::post('articles/search', 'ArticleController@search');
Route::post('articles/tags/attach','ArticleController@attachTag');
Route::post('articles/new','ArticleController@new');
Route::patch('articles/update','ArticleController@update');

// ArticleSteps
Route::post('article-steps/new','ArticleStepController@create');
Route::patch('article-steps/update','ArticleStepController@update');
Route::delete('article-steps/delete','ArticleStepController@delete');

// Tags
Route::get('tags/all','TagController@all');
Route::post('tags/new','TagController@create');
Route::delete('tags/delete','TagController@delete');
Route::patch('tags/update','TagController@update');

// Pictures
Route::get('pictures/{file}',function($file)
{
    $path = Storage::disk('pictures')->getDriver()->getAdapter()->applyPathPrefix($file);
    return response()->file($path);
});

// Server time
Route::get('time',function()
{
    // Carbon only tracks accuracy of seconds so convert result to millis.
    // return \Carbon\Carbon::now()->timestamp * 1000;
    return \Carbon\Carbon::now();
});

// Authentication
Auth::routes();