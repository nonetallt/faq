// ES 2015 module imports

import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import jQuery from 'jquery';
import moment from 'moment';
import lodash from 'lodash';

// Set variables to window for ease of usage
window.$ = window.jQuery = jQuery;
window._ = window.lodash = lodash;
window.Vue = Vue;
window.axios = axios;
window.moment = moment;

// Use router plugin.
Vue.use(VueRouter);

require('bootstrap-sass');

window.axios.defaults.headers.common =
{
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};