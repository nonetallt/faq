export default class JmpModal
{
    constructor(settings)
    {
        this.settings = settings;
        this.isOpen = false;
        this.closable = true;
    }

    activeClass()
    {
        return 'is-active';
    }

    baseClasses()
    {
        return ['modal'];
    }

    class()
    {
        let classObject = {};
        let baseClasses = this.baseClasses();
        
        // Add base classes.
        for(let n = 0; n <= baseClasses.length-1; n++)
        {
            classObject[baseClasses[n]] = true;
        }

        // Add active class.
        classObject[this.activeClass()] = this.isOpen;
        return classObject;
    }

    open()
    {
        this.isOpen = true;
    }

    close()
    {
        this.isOpen = false;
    }
}