export default class JmpField
{
    constructor(type,name,settings)
    {
        this.originalSettings = settings;
        this.settings = settings;
        this.name = name;
        this.type = this.checkType(type);
        this.applySettings();
    }

    applySettings()
    {
        for(let setting in this.defaultSettings())
        {
            this[setting] = this.checkSetting(setting);
        }
    }

    checkSetting(setting)
    {
        if(this.settings[setting] !== undefined)
        {
            return this.settings[setting];
        }
        return this.defaultSettings()[setting];
    }

    defaultSettings()
    {
        return {
            loading: false,
            disabled: false,
            validated: false,
            errors: [],
            selectOptions: [],
            placeholder: this.name,
            // Uppercase label by default.
            label: (this.name.charAt(0).toUpperCase() + this.name.slice(1)),
            value: ''
        }
    }

    types()
    {
        return ['text','textarea','select','file'];
    }

    removeSelectOptions(options)
    {
        this.selectOptions = _.difference(this.selectOptions,options);
    }

    hasErrors()
    {
        if(this.errors.length > 0)
        {
            return true;
        }
        return false;
    }

    checkType(type)
    {
        if(this.types().indexOf(type) === false)
        {
            throw new Error('Field type is not allowed. Allowed types: '+this.types().join());
        }
        return type;
    }

    reset()
    {
        this.value = '';
        this.settings = this.originalSettings;
        this.applySettings();
    }
}