export default class JmpErrors
{
    constructor()
    {
        this.errors = {};
    }

    get(field)
    {
        // Check if error for field exists.
        if(this.errors[field])
        {
            return this.errors[field];
        }
        return [];
    }

    messages()
    {
        let messages = [];
        for(let field in this.errors)
        {
            let errorsForField = this.get(field);
            messages = messages.concat(errorsForField);
        }
        return messages;
    }

    setErrors(errors)
    {
        this.errors = errors;
    }

    clearAll()
    {
        for(let field in this.errors)
        {
           this.clear(field);
        }
    }

    clear(field)
    {
        delete this.errors[field];
    }

    exists(field)
    {
        if(this.errors[field])
        {
            return true;
        }
        return false;
    }

    exist()
    {
        return Object.keys(this.errors).length > 0;
    }
}