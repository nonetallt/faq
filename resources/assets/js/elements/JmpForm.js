import Errors from './JmpErrors';
import Field from './JmpField';

export default class JmpForm
{
    constructor()
    {
        this.fields = [];
        this.submitting = false;
        this.errors = new Errors();
    }


    addField(type,name,settings)
    {
        if(settings === undefined)
        {
            settings = {};
        }

        this.fields.push(new Field(type,name,settings));
        // Chainable.
        return this;
    }

    data()
    {
        let data = new FormData();
        // Field data.
        for(let n = 0; n <= this.fields.length-1; n++)
        {
            // Copy field data to a separate object and return it.
            let field = this.fields[n];
            //console.log('setting key \''+field.name+'\' as \''+field.value+'\'')

            if(field.type === 'file')
            {
                try
                {
                    let blob = this.dataURItoBlob(field.value);
                    let file = new File([blob], "image");
                    data.append(field.name,file);
                }
                catch(error)
                {
                    console.log('file could not be parsed');
                }
                continue;
            }
            data.append(field.name,field.value);
            //data[field.name] = field.value;
        }
        return data;
    }

    getField(name)
    {
        for(let n = 0; n <= this.fields.length-1; n++)
        {
            let field = this.fields[n];
            if(field.name === name)
            {
                return field;
            }
        }
        return null;
    }

    reset()
    {
        // Clear fields.
        for(let n = 0; n <= this.fields.length-1; n++)
        {
            this.fields[n].reset();
        }
        // Clear status.
        this.submitting = false;
    }

    // Submit proxies.
    submit(method,url,extraData)
    {
        console.log('submit('+method+') called url: '+url);
        this.submitting = true;
        let data = this.data();

        if(extraData !== undefined)
        {
            for(let key in extraData)
            {
                data.append(key,extraData[key]);
            }
        }

        console.log(data);

        return new Promise((resolve,reject) => {
            axios(
            {
                method: method,
                url: url,
                data: data
            })
            .then(response => {
                this.submitting = false;
                resolve(response);
            })
            .catch(error => {
                this.submitting = false;
                this.errors.setErrors(error.response.data);

                // Set errors to all fields where they exist.
                for(let n = 0; n <= this.fields.length-1; n++)
                {
                    this.fields[n].errors = this.errors.get(this.fields[n].name);
                }
                reject(error);
            });
        });
    }

    get(url,extraData)
    {
        return this.submit('get',url,extraData);
    }
    post(url,extraData)
    {
        return this.submit('post',url,extraData);
    }
    patch(url,extraData)
    {
        return this.submit('patch',url,extraData);
    }
    delete(url,extraData)
    {
        return this.submit('delete',url,extraData);
    }

    dataURItoBlob(dataURI)
    {
        // convert base64 to raw binary data held in a string
        // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
        let byteString = atob(dataURI.split(',')[1]);

        // separate out the mime component
        let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to an ArrayBuffer
        let ab = new ArrayBuffer(byteString.length);
        let ia = new Uint8Array(ab);
        for (let i = 0; i < byteString.length; i++)
        {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ab], {type: mimeString});
    }
}