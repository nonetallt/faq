import AbstractionError from './AbstractionError';

export default class Abstract
{
    constructor(methods)
    {

        if (new.target instanceof Abstract) 
        {
            throw new AbstractionError("Cannot construct Abstract instances directly");
        }

        for(var n = 0; n < methods.length; n++)
        {
            if (!_.has(this.__proto__.constructor,methods[n]))
            {
                throw new AbstractionError('Class must implement abstract method \''+methods[n]+'\'');
            }
        }
    }
}