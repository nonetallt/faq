import Model from './Model';

export default class ArticleHead extends Model
{
    constructor(component)
    {
        super(component);
        this.loadingTags = true;
        this.loadTags();
    }

    loadTags()
    {
        axios.post('articles/tags',{
            id: this.id
        })
        .then(response => {
            this.tags = response.data;
            this.loadingTags = false;
        })
        .catch(error => {
            this.tags = ['error loading tags'];
            this.loadingTags = false;
        });
    }

    static properties()
    {
        return {
            id: {
                required: true,
                type: Number
            },
            title: {
                required: false,
                type: String
            },
            description: {
                required: false,
                type: String
            },
            platform: {
                required: false,
                type: String
            },
            /*
            created_at: {
                required: false,
                type: String
            },
            */
            updated_at: {
                required: false,
                type: String
            }
        };
    }
}