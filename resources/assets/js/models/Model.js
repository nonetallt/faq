import Abstract from './Abstract';

export default class Model extends Abstract
{
    constructor(component)
    {
        // Define abstract methods.
        super(['properties']);
        this.originalFields = [];
        this.component = component;
        this.loading = false;
        this.initializeFields();
    }

    initializeFields()
    {
        let component = this.component;
        for (let property in component._props) 
        {
            this.originalFields.push(property);
            this[property] = component[property];
        }
    }

    resetFields()
    {
        for (let property in originalFields) 
        {
            delete this.property;
        }
    }

    missingProperties()
    {
        let component = this.component;
        let missingProps = [];

        for (let property in component._props) 
        {
            if(component[property] === undefined)
            {
                missingProps.push(property);
            }
        }
        return missingProps;
    }

    load(method,link)
    {
        if(method === undefined || link === undefined)
        {
            throw new Error('Method load requires both http method and url parameters.');
        }

        let missingProps = this.missingProperties();
        if(missingProps.length < 1)
        {
            return;
        }
        this.loading = true;
        
        axios(
        {
            method: method,
            url: link,
            data: {
                id: this.id,
                columns: missingProps
            }
        })
        .then(response => {

            for (let n = 0; n <= missingProps.length-1; n++)
            {
                let column = missingProps[n];
                    //console.log('Setting '+column+' to '+ response.data[column]);
                    this[column] = response.data[column];
                }
                this.loading = false;
            })
        .catch(error => {
            this.loading = false;
        });
    }
}