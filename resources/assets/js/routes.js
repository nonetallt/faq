import VueRouter from 'vue-router';


// .vue extension can be omitted when using mix.
let routes = 
    [
        {
            path: '/',
            redirect: 'home'
        },
        {
            path: '/home',
            component: require('./views/Home.vue')
        },
        {
            path: '/search',
            component: require('./views/SearchResults.vue')
        },
        {
            path: '/tags',
            component: require('./views/Tags.vue')
        },
        {
            path: '/articles',
            component: require('./views/Article.vue')
        },
        {
            path: '/articles.newest',
            component: require('./views/NewestArticles.vue')
        },
        {
            path: '/articles.updated',
            component: require('./views/LastUpdatedArticles.vue')
        },
        {
            path: '/articles.my',
            component: require('./views/MyArticles.vue')
        }
    ];


export default new VueRouter(
{
    routes,
    linkActiveClass: 'is-active'
});
