
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap';
import AppNav from './components/app/AppNav.vue';
import AppFooter from './components/app/AppFooter.vue';
import router from './routes';

const app = new Vue(
{
    el: '#app',
    router,
    components: 
    { 
        AppNav,
        AppFooter
    }
});
