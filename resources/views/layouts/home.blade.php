<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta author="JMP">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{mix('css/app.css')}}"/>

    <!-- Scripts -->
    <script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>

<body>
    <div id="app">
        <app-nav></app-nav>
        <section class="section">
            <div class="container">
                <div class="content">
                    <router-view></router-view>
                </div>
            </div>
        </section>
        <app-footer></app-footer>
    </div>
    <script type='text/javascript' src="{{mix('js/app.js')}}"></script>
</body>
</html>